package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        try {
            try {  //если вылетает NullPointerException или OutOfMemoryError перехватываем и...
                Collections.sort(inputNumbers);
            } catch (NullPointerException | OutOfMemoryError e) {
                throw new CannotBuildPyramidException();    //...и выбрасываем CannotBuildPyramidException
            }

            int summ = 0;                       //длина массива
            int counter = 0;                    //высота массива

            // определение размерности массива
            for (int i = 1; i < inputNumbers.size(); i++) {
                if (summ > inputNumbers.size()) {
                    throw new CannotBuildPyramidException();
                } else if (summ == inputNumbers.size()) {
                    i -= 2;                     //откатываемся к предпоследней строке треугольника
                    summ = counter + i;         //длина массива равна сумме двух последних строк треугольника
                    break;
                } else {
                    counter++;                   //высота массива равна длине последней строки треугольника
                    summ += i;
                }
            }

            int [][] readyPyramid = new int[counter][summ];
            //заполнение массива входящими значениями в виде пирамиды
            for (int i = 0, k = 0; i < counter; i++) {
                for (int j = summ / 2 - i; j <= summ / 2 + i; j++) {
                    readyPyramid [i][j] = inputNumbers.get(k);
                    j++;
                    k++;
                }
            }
            return readyPyramid;
        } catch (CannotBuildPyramidException e) {
            throw new CannotBuildPyramidException();
        }
    }
}
