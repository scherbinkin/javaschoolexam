package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */

    public String evaluate(String statement) {

        String output = getPostFix(statement); //Преобразовываем инфиксную запись в постфиксную (О.П.Н)

        return String.valueOf(counting(output)); //Решаем полученное выражение и возвращаем результат
    }

    //Возвращаем true, если проверяемый символ - "пробел"
    static private boolean isSpace(char ch) {
        return ch == ' ';
    }
    //Возвращаем true, если проверяемый символ - оператор
    static private boolean isOperator(char operator) {
        return operator == '+' || operator == '-' || operator == '*' || operator == '/';
    }
    //Возвращаем приоритет оператора
    static private byte getPriority(char priority) {
        switch (priority) {
            case '(': return 0;
            case ')': return 1;
            case '-': return 3;
            case '+': return 2;
            default: return 4; //    * и /
        }
    }

    static private String getPostFix(String input) {
        String output = "";                             //Строка для хранения постфиксного выражения
        Stack<Character> operatorsStack = new Stack<>(); //Стек для хранения операторов

        for (int i = 0; i < input.length(); i++) {//

            if (isSpace((char) input.charAt(i))) {       //пропускаем пробелы
                continue;
            }
            //Если символ - цифра, то считываем все число
            if (Character.isDigit(input.charAt(i))) {
                //Читаем до оператора или конца строки, чтобы получить число
                while (!isOperator(input.charAt(i))) {
                    output += input.charAt(i); //конкатенируем
                    i++;
                    if (i == input.length()) {
                        i--;
                        break;
                    }
                }
                output += " ";
            }
            //Если символ - оператор
            if (isOperator(input.charAt(i))) {
                if (input.charAt(i) == '(') {               //Если символ открывающая скобка
                    operatorsStack.push(input.charAt(i));   //Записываем её в стек
                } else if (input.charAt(i) == ')') {        //Если символ закрывающая скобка
                    //Выписываем все операторы из конца стека до открывающей скобки в строку
                    char operatorsBeforeBracket = operatorsStack.pop();

                    while (operatorsBeforeBracket != '(') {
                        output += operatorsBeforeBracket + " ";
                        operatorsBeforeBracket = operatorsStack.pop();
                    }
                    operatorsStack.pop();
                } else {    //Если любой другой оператор
                    //Если в стеке есть элементы или приоритет нашего оператора меньше или равен приоритету оператору в конце стека
                    if (!operatorsStack.empty() && getPriority(input.charAt(i)) <= getPriority(operatorsStack.peek())) {
                        //То добавляем последний оператор из стека в строку с выражением
                        output += operatorsStack.pop().toString() + " ";
                        operatorsStack.push(input.charAt(i));
                    } else { //Если стек пуст, или же приоритет оператора выше - добавляем оператор в конец стека
                        operatorsStack.push(input.charAt(i));
                    }
                }
            }
        }
        //Когда прошли по всем символам, выкидываем из стека все оставшиеся там операторы в строку
        while (!operatorsStack.empty()) {
            output += operatorsStack.pop() + " ";
        }
        return output; //Возвращаем выражение в постфиксной записи
    }

    static private int counting(String input) {
        int result = 0;
        Stack<Integer> reversePolNot = new Stack<>(); //стек для калькуляции О.П.Н.

        for (int i = 0; i < input.length(); i++) {
            //Если символ - цифра, то читаем все число и записываем в конец стека
            if (Character.isDigit(input.charAt(i))) {
                String number = "";

                while (!isSpace(input.charAt(i))) {
                    number +=  input.charAt(i);
                    i++;
                    if (i == input.length()) {
                        break;
                    }
                }
                reversePolNot.push(Integer.valueOf(number)); //Записываем в стек
                // i--;
            } else if (isOperator(input.charAt(i))) {    //Если символ - оператор
                //Берем два последних значения из стека
                int second = reversePolNot.pop();
                int first = reversePolNot.pop();

                switch (input.charAt(i)) {  //И производим над ними действие, согласно оператору
                    case '+': result = first + second; break;
                    case '-': result = first - second; break;
                    case '*': result = first * second; break;
                    case '/': result = first / second; break;
                }
                reversePolNot.push(result); //Результат вычисления записываем обратно в стек
            }
        }
        return reversePolNot.peek(); //Забираем результат всех вычислений из стека и возвращаем его
    }
}
