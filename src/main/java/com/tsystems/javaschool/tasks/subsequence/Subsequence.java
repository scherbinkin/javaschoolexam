package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    boolean find(List x, List y) {
        try {
            if (x.isEmpty() & !y.isEmpty()) {    //если  X пустая строка, то условно можно считать, что такая ...
                return true;                    //...последовательность существует во второй (не пустой) строке.
            } else {                            // Если null то exception!
                int counter = 0;
                for (Object check : y) {        //если  Y пустая строка то возвращаем рез-т сравнения (false),
                    if (counter == x.size()) {
                        break;
                    } else if (x.get(counter).equals(check)) {
                        counter++;                //счетчик совпадений
                    }
                }
                return counter == (x.size());
            }
        } catch (Exception e) {
            throw new IllegalArgumentException();
        }
    }
}
